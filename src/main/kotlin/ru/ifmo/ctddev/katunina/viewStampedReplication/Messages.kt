package ru.ifmo.ctddev.katunina.viewStampedReplication

import com.fasterxml.jackson.annotation.JsonTypeInfo
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper


/**
 * Messages between [Node]s and Clients.
 *
 * Overriding of toString is used to serialize messages into
 * string representation and send them.
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS)
interface Message {
    companion object {
        val mapper = jacksonObjectMapper()
    }
}

//region Node messages

data class DoViewChangeMessage(val senderViewNumber: Int,
                               val log: List<Replica.LogEntry>,
                               val senderLatestNormalViewNumber: Int,
                               val senderOpNumber: Int,
                               val commitNumber: Int,
                               val senderId: Int) : Message {
    override fun toString() = Message.mapper.writeValueAsString(this)!!
}

data class StartViewMessage(val viewNumber: Int,
                            val log: List<Replica.LogEntry>,
                            val opNumber: Int,
                            val commitNumber: Int,
                            val leaderId: Int) : Message {
    override fun toString() = Message.mapper.writeValueAsString(this)!!
}

data class StartViewChangeMessage(val newViewNumber: Int,
                                  val newLeaderId: Int,
                                  val senderId: Int) : Message {
    override fun toString() = Message.mapper.writeValueAsString(this)!!
}

data class FaultyNodesMessage(val faultyNodes: Set<Int>) : Message {
    override fun toString() = Message.mapper.writeValueAsString(copy(faultyNodes.toSet()))!!
}

data class SendResultToClientMessage(val viewNumber: Int,
                                     val clientId: Int,
                                     val result: String,
                                     val clientMessage: ClientRequest) : Message {
    override fun toString() = Message.mapper.writeValueAsString(this)!!
}

data class TransferClientMessage(val destination: Int,
                                 val clientId: Int,
                                 val clientRequestNumber: Int,
                                 val clientMessage: String,
                                 val viewNumber: Int) : Message {
    override fun toString() = Message.mapper.writeValueAsString(this)!!
}

/**
 * Message which is sent first in order to establish connection between [Node]s.
 */
data class PrepareMessage(val viewNumber: Int,
                          val clientId: Int,
                          val requestNumber: Int,
                          val clientMessage: ClientRequest,
                          val opNumber: Int,
                          val commitNumber: Int) : Message {
    override fun toString() = Message.mapper.writeValueAsString(this)!!
}

data class RecoveryMessage(val viewNumber: Int,
                           val id: Int,
                           val nonce: Long) : Message {
    override fun toString() = Message.mapper.writeValueAsString(this)!!
}

data class YouAreFaultyMessage(val viewNumber: Int,
                               val yourViewNumber: Int, val yourClientRequest: ClientRequest?) : Message {
    override fun toString() = Message.mapper.writeValueAsString(this)!!
}

data class RecoveryResponse(val viewNumber: Int,
                            val nonce: Long,
                            val log: List<Replica.LogEntry>?,
                            val opNumber: Int?,
                            val commitNumber: Int?,
                            val leaderId: Int?) : Message {
    override fun toString() = Message.mapper.writeValueAsString(this)!!
}

data class PrepareOKMessage(val viewNumber: Int,
                            val opNumber: Int,
                            val backUpNumber: Int) : Message {
    override fun toString() = Message.mapper.writeValueAsString(this)!!
}

data class CommitMessage(val viewNumber: Int,
                         val commitNumber: Int) : Message {
    override fun toString() = Message.mapper.writeValueAsString(this)!!
}

data class PrepareCommitMessage(val viewNumber: Int) : Message {
    override fun toString() = Message.mapper.writeValueAsString(this)!!
}

data class NodeMessage(val fromId: Int) : Message {
    override fun toString() = "node $fromId"
}

/**
 * Request for checking connectivity between [Node]s.
 * [PongMessage] is the right response.
 */
class PingMessage() : Message {
    override fun toString() = Message.mapper.writeValueAsString(this)!!
}

/**
 * Response for [PingMessage] which shows positive connectivity.
 */
class PongMessage() : Message {
    override fun toString() = Message.mapper.writeValueAsString(this)!!
}

//endregion Node messages

//region Client requests

/**
 * Sub-hierarchy of client requests.
 *
 * Client messages are only received and dispatched to replicas and are never sent
 * themselves but can still be sent as payload.
 *
 */
interface ClientRequest : Message {

    val clientId: Int
    val requestNumber: Int

    companion object {
        fun parse(clientId: Int, requestNumber: Int, parts: List<String>): ClientRequest? =
                when (parts[0]) {
                    "get" -> GetRequest(clientId, requestNumber, parts[1])
                    "set" -> SetRequest(clientId, requestNumber, parts[1], parts.drop(2).joinToString(" "))
                    "delete" -> DeleteRequest(clientId, requestNumber, parts[1])
                    "ping" -> PingRequest(clientId, requestNumber)
                    else -> {
                        NodeLogger.logErr("Invalid client request: ${parts.joinToString(" ")}"); null
                    }
                }

    }

}

data class PingRequest(override val clientId: Int,
                       override val requestNumber: Int) : ClientRequest {
    override fun toString() = "ping client$clientId, request number is $requestNumber"
}

data class GetRequest(override val clientId: Int,
                      override val requestNumber: Int,
                      val key: String) : ClientRequest {
    override fun toString() = "get $key"
}

data class SetRequest(override val clientId: Int,
                      override val requestNumber: Int,
                      val key: String,
                      val value: String) : ClientRequest {
    override fun toString() = "set $key $value"
}

data class DeleteRequest(override val clientId: Int,
                         override val requestNumber: Int,
                         val key: String) : ClientRequest {
    override fun toString() = "delete $key"
}


//endregion Client requests


/**
 * Never received by [Node]s.
 * The only usage is for sending responses to the Clients.
 */
class TextMessage(val text: String) : Message {
    override fun toString() = text
}

