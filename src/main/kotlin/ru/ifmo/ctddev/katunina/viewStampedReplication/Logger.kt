package ru.ifmo.ctddev.katunina.viewStampedReplication

import java.util.logging.Level
import java.util.logging.Logger

/**
 * Provides support for logging events specific for [Node]s
 */

open class NodeLogger() {
    private val logger = Logger.getLogger("Node")

    private fun Message.isLoggable() =
            this !is PingMessage &&
                    this !is PongMessage

    private fun printLeader(isLeader: Boolean): String {
        if (isLeader) return "LEADER" else return ""
    }

    fun logMsgIn(m: Message, fromId: Int, isLeader: Boolean, status: Status?) {
        if (m.isLoggable() && m !is CommitMessage)
            logger.log(Level.INFO, ">> from $fromId ${printLeader(isLeader)}: $m")
    }

    fun logMsgOut(m: Message, toId: Int, isLeader: Boolean, status: Status?) {
        if (m.isLoggable() && m !is CommitMessage)
            logger.log(Level.INFO, "<< to $toId ${printLeader(isLeader)}: $m STATUS = $status")
    }

    fun logMsgHandle(m: Message) {
        if (m.isLoggable() && m !is FaultyNodesMessage)
            logger.log(Level.INFO, ".. handling: $m")
    }

    fun logProtocol(s: String, isLeader: Boolean) {
        logger.log(Level.INFO, "## ${printLeader(isLeader)} $s")
    }

    fun logConn(s: String) {
        logger.log(Level.INFO, "   $s")
    }

    fun logErr(s: String, t: Throwable? = null) {
        logger.log(Level.SEVERE, "!! $s", t)
    }

    companion object : NodeLogger() {}
}