package ru.ifmo.ctddev.katunina.viewStampedReplication

import java.nio.charset.Charset
import java.util.*

/**
 * Stores nodes configuration which consists of:
 * @property addresses mapping of node ids to network addresses on which the nodes can be found
 * @property timeout idleness limit for nodes communication
 */

data class Configuration(val addresses: Map<Int, String>,
                         val timeout: Long
) {
    fun port(id: Int): Int {
        if (id !in addresses)
            throw IllegalArgumentException("ID out of configuration.")
        val parts = addresses[id]!!.split(":")
        return parts[1].toInt()
    }

    fun address(id: Int): String {
        if (id !in addresses)
            throw IllegalArgumentException("ID out of configuration.")
        val parts = addresses[id]!!.split(":")
        return parts[0]
    }

    val nodesCount: Int
        get() = addresses.size

    val ids: List<Int>
        get() = (1..nodesCount).toList()

    val consensusSize: Int
        get() = (nodesCount + 1)/2

    companion object {
        fun readDkvsProperties(filename: String = "dkvs.properties"): Configuration {

            val NODE_ADDRESS_PREFIX = "node"

            val input = Configuration::class.java.classLoader.getResourceAsStream(filename)
            val props = Properties()
            props.load(input)

            val timeout = (props["timeout"] as String).toLong()
            val addresses = hashMapOf<Int, String>()
            for ((k, v) in props.entries) {
                if (k !is String || v !is String)
                    continue
                if (k.startsWith(NODE_ADDRESS_PREFIX)) {
                    val parts = k.split(".")
                    val id = parts[1].toInt()
                    addresses[id] = v
                }
            }

            return Configuration(Collections.unmodifiableMap(addresses), timeout)
        }
    }
}

val GLOBAL_CONFIG: Configuration = Configuration.readDkvsProperties()

val CHARSET: Charset = Charset.forName("UTF-8")