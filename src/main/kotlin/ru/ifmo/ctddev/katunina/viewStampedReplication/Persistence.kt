package ru.ifmo.ctddev.katunina.viewStampedReplication


import java.io.File
import java.io.FileWriter
import java.util.*

/**
 * Holds the disk storage for node.
 */

class Persistence(nodeId: Int) {

    val fileName: String = "dkvs_$nodeId.log"
    private val writer = FileWriter(fileName, true).buffered()

    @Volatile var keyValueStorage = HashMap<String, String>()

    fun saveToDisk(s: String) {
        with(writer) {
            write(s)
            newLine()
            flush()
        }
    }

    var commitNumber = 0
    var opNumber = 0
    var diskLog = ArrayList<Replica.LogEntry>()

    init {
        val file = File(fileName)
        if (file.exists()) {
            val reader = file.reader().buffered()
            val lines = ArrayList<String>()
            for (l in reader.lines())
                lines.add(l)

            val storage = hashMapOf<String, String>()
            var i =1
            loop@ for (l in lines) {
                val parts = l.split(' ')
                val requestNumber = parts[3].toInt()
                val clientId = parts[1].toInt()
                if (parts.size >=5 && parts[4] == "set"){
                    val setReq = SetRequest(clientId, requestNumber, parts[5], parts[6])
                    diskLog.add(Replica.LogEntry(i, requestNumber, clientId, setReq))
                    storage.put(parts[5], parts[6])
                }
                else if (parts.size >= 5 && parts[4] == "delete"){
                       val deleteReq = DeleteRequest(clientId,requestNumber,parts[5])
                       diskLog.add(Replica.LogEntry(i, requestNumber, clientId, deleteReq))
                       storage.remove(parts[5])
                }
                else throw IllegalArgumentException("No such client request.")
                i++
            }
            keyValueStorage = storage
            commitNumber = lines.size
            opNumber = commitNumber
        }
    }

}

