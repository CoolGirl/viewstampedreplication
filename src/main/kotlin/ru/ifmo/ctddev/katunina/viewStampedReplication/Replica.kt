package ru.ifmo.ctddev.katunina.viewStampedReplication

import java.util.*

enum class Status {NORMAL, VIEWCHANGE, RECOVERING }

/**
 * Replicas are bridges between Clients and abstraction layer which
 * actually makes consensus decisions.
 *
 *
 * @param id Replica identifier, unique across the protocol instance.
 * @param send (nodeId, message) Way to send messages to the other nodes.
 *
 *
 *
 * @property clientIdToRequests [ClientRequest]s which haven't been proposed yet
 * @property performed [ClientRequest]s which have already been [performClientRequest]ed.
 */

class Replica(val id: Int,
              val send: (nodeId: Int, Message) -> Unit,
              val sendToClient: (clientId: Int, text: String) -> Unit,
              val persistence: Persistence
) {

    private var viewNumber = 0
    private var status: Status = Status.NORMAL
    private var opNumber = persistence.opNumber
    private var commitNumber = persistence.commitNumber
    private var localLeaderId = 1
    private var latestViewNumberWithNormalStatus = 0

    private var log = persistence.diskLog
    private var messagesWithoutAnswer = LinkedHashSet<Message>()
    private val lastRequestByClientId = HashMap<Int, RequestInfo>()
    private val allIds = GLOBAL_CONFIG.ids

    private fun isLeader(): Boolean = localLeaderId == id

    data class LogEntry(val opNumber: Int, val requestNumber: Int, val clientId: Int, val clientMessage: ClientRequest)

    private data class RequestInfo(val requestNumber: Int, val clientMessage: ClientRequest, val result: String? = null)

    private val state: MutableMap<String, String> = persistence.keyValueStorage

    private val performed = HashSet<RequestInfo>()

    private fun performClientRequest(c: RequestInfo) {
        NodeLogger.logProtocol("PERFORMING $c at $id", isLeader())
        val pair = sendResultIfFirstTime(c.clientMessage)
        val awaitingClient = pair.first
        val result = pair.second
        destination = id
        performed.add(c)
        val temp = lastRequestByClientId[awaitingClient]!!.clientMessage
        lastRequestByClientId.put(awaitingClient, RequestInfo(c.requestNumber, temp, result))
        if (log.size < 2 || log.last() != log[log.lastIndex - 1]) {
            if (c.clientMessage is SetRequest) {
                persistence.saveToDisk("clientId $awaitingClient requestNumber ${c.requestNumber}" +
                        " set ${c.clientMessage.key} ${c.clientMessage.value}")
            } else if (c.clientMessage is DeleteRequest) {
                persistence.saveToDisk("clientId $awaitingClient requestNumber ${c.requestNumber}" +
                        " delete ${c.clientMessage.key}")
            }
            commitNumber++
        } else {
            log.dropLast(1)
        }
    }

    private fun sendResultIfFirstTime(clientMessage: ClientRequest):
            Pair<Int, String?> {
        var result: String? = null
        if (clientMessage is SetRequest) {
            state[clientMessage.key] = clientMessage.value
            result = "STORED"
        } else if (clientMessage is DeleteRequest) {
            val found = (state.remove(clientMessage.key)) != null
            result = if (found) "DELETED" else "NOT_FOUND"
        }
        val awaitingClient = clientMessage.clientId
        if (isLeader() /*&& !doNotRequireAnswer.contains(clientMessage.requestNumber*/) {
            if (destination != id) {
                send(destination, SendResultToClientMessage(viewNumber, awaitingClient, result!!, clientMessage))
            } else {
                sendToClientAndDropFromHaveNotAnswer(clientMessage, awaitingClient, result!!)
            }
        }
        return Pair(awaitingClient, result)
    }

    private fun sendToClientAndDropFromHaveNotAnswer(clientMessage: ClientRequest,
                                                     awaitingClient: Int, result: String) {
        sendToClient(awaitingClient, result)
        messagesWithoutAnswer.remove(clientMessage)
    }

    private val preparedOKNodesNumberByOpNumber = HashMap<Int, Int>()
    private var startViewChangeSet = HashSet<Int>()
    private var doViewChangeNumberSet = HashSet<Int>()
    private val clientIdByOpNumber = HashMap<Int, Int>()
    private var doViewChangeInfo = DoViewChangeInfo(latestViewNumberWithNormalStatus, log, opNumber, commitNumber)

    private class DoViewChangeInfo(var largestNormalStateViewNumber: Int, var bestLog: ArrayList<LogEntry>,
                                   var largestOpNumber: Int, var largestCommitNumber: Int) {
    }

    private var doneRecovering = false

    private fun nextLeader(): Int = localLeaderId % GLOBAL_CONFIG.nodesCount + 1

    private var destination = id
    private var recoveryResponses = 0
    private var oldCommitNumber = -1

    private fun recover(): Unit {
        status = Status.RECOVERING
        nonce = System.currentTimeMillis()
        NodeLogger.logProtocol("RECOVERING", isLeader())
        oldCommitNumber = commitNumber
        allIds.forEach {
            if (id != it) {
                val recoveryMessage = RecoveryMessage(viewNumber, id, nonce)
                NodeLogger.logMsgOut(recoveryMessage, it, isLeader(), status)
                send(it, recoveryMessage)
            }
        }
    }


    /**
     * Should be called from the replica's container to pass to the replica each message
     * addressed to it.
     * @param message Message that should be handled by the replica.
     */
    fun receiveMessage(message: Message) {
        when (message) {
            is FaultyNodesMessage ->
                if (message.faultyNodes.contains(localLeaderId) && status == Status.NORMAL)
                    startViewChange(viewNumber + 1, id)
                else if (isLeader()) {
                    allIds.forEach { it -> send(it, CommitMessage(viewNumber, commitNumber)) }
                }
            is RecoveryMessage -> handleRecoveryMessage(message)
            is RecoveryResponse -> handleRecoveryResponse(message.leaderId!!, message)
            is StartViewChangeMessage -> handleStartViewChangeMessage(message)
            is StartViewMessage -> handleStartViewMessage(message)
            is DoViewChangeMessage -> handleDoViewChangeMessage(message)
            is SendResultToClientMessage -> sendToClientAndDropFromHaveNotAnswer(message.clientMessage,
                    message.clientId, message.result)
            is ClientRequest -> handleClientRequest(message)
            is TransferClientMessage -> handleTransferClientMessage(message)
            is PrepareMessage -> handlePrepareMessage(message)
            is CommitMessage -> handleCommitMessage(message)
            is PrepareOKMessage -> handlePrepareOKMessage(message)
            is PrepareCommitMessage -> handlePrepareCommitMessage(message)
        }
    }

    //region commit
    private fun handlePrepareCommitMessage(message: PrepareCommitMessage) {
        if (isLeader()) {
            NodeLogger.logProtocol("PrepareCommitMessage ", true)
            NodeLogger.logMsgIn(message, id, isLeader(), status)
            allIds.forEachIndexed { i, it ->
                val commitMsg = CommitMessage(viewNumber, commitNumber)
                send(it, commitMsg)
                NodeLogger.logMsgOut(commitMsg, i, isLeader(), status)
            }
        }
    }

    private fun handleCommitMessage(message: CommitMessage) {
        if (status != Status.VIEWCHANGE) {
            if (!isLeader()) {
                NodeLogger.logProtocol("CommitMessage ", false)
                NodeLogger.logMsgIn(message, localLeaderId, isLeader(), status)
                if (this.commitNumber < message.commitNumber - 1 || message.viewNumber > viewNumber) {
                    recover()
                    return
                }
                if (this.commitNumber < message.commitNumber && message.viewNumber == viewNumber) {
                    System.out.println("${message.commitNumber}, ${clientIdByOpNumber[message.commitNumber]}," +
                            "${lastRequestByClientId[clientIdByOpNumber[message.commitNumber]]}")
                    if (lastRequestByClientId[clientIdByOpNumber[message.commitNumber]]!!.result == null) {
                        performClientRequest(lastRequestByClientId[clientIdByOpNumber[message.commitNumber]]!!)
                    }
                } else if (this.commitNumber > message.commitNumber || message.viewNumber < viewNumber) {
                    startViewChange(viewNumber + 1, id)
                }
            } else if (viewNumber < message.viewNumber) {
                recover()
            }
        }

    }
    //endregion

    //region prepare
    private fun handlePrepareOKMessage(message: PrepareOKMessage) {
        if (isLeader()) {
            NodeLogger.logProtocol("PrepareOKMessage", false)
            NodeLogger.logMsgIn(message, message.backUpNumber, isLeader(), status)
            val fromOpNumber = message.opNumber
            preparedOKNodesNumberByOpNumber.compute(fromOpNumber, { key, value ->
                if (value != null) value + 1 else 1
            })
            NodeLogger.logProtocol("preparedOK = ${preparedOKNodesNumberByOpNumber[fromOpNumber]}", isLeader())
            if (preparedOKNodesNumberByOpNumber[fromOpNumber] == GLOBAL_CONFIG.consensusSize - 1) {
                val clientRequestInfo = lastRequestByClientId[clientIdByOpNumber[fromOpNumber]]
                if (clientRequestInfo!!.result == null) {
                    performClientRequest(clientRequestInfo)
                }
                receiveMessage(PrepareCommitMessage(viewNumber))
            }
        }
    }

    private fun handlePrepareMessage(message: PrepareMessage) {
        if (!isLeader()) {
            NodeLogger.logProtocol("PrepareMessage", isLeader())
            NodeLogger.logMsgIn(message, localLeaderId, isLeader(), status)
            val fromViewNumber = message.viewNumber
            val fromClientId = message.clientId
            val fromRequestNumber = message.requestNumber
            val fromClientMessage = message.clientMessage
            val fromOpNumber = message.opNumber
            val fromCommitNumber = message.commitNumber
            NodeLogger.logProtocol("commit number = $commitNumber, fromCommitNumber = $fromCommitNumber", isLeader())
            if (this.commitNumber < fromCommitNumber) {
                NodeLogger.logProtocol("RECOVERING", isLeader())
                status = Status.RECOVERING
                recover()
            }
            opNumber = fromOpNumber
            if (this.commitNumber > fromCommitNumber) {
                val msgToLeader = PrepareOKMessage(viewNumber, fromOpNumber, id)
                send(localLeaderId, msgToLeader)
                return
            }
            log.add(LogEntry(fromOpNumber, fromRequestNumber, fromClientId, fromClientMessage))
            lastRequestByClientId.put(fromClientId,
                    RequestInfo(fromRequestNumber, fromClientMessage, null))
            clientIdByOpNumber.put(fromOpNumber, fromClientId)
            val msgToLeader = PrepareOKMessage(viewNumber, fromOpNumber, id)
            send(localLeaderId, msgToLeader)
            NodeLogger.logMsgOut(msgToLeader, localLeaderId, isLeader(), status)
        }
    }
    //endregion

    //region transfer
    private fun handleTransferClientMessage(message: TransferClientMessage) {
        NodeLogger.logProtocol("TransferClientMessage", isLeader())
        val clientMessage = ClientRequest.parse(message.clientId, message.clientRequestNumber,
                message.clientMessage.split(" "))!!
        if (isLeader()) {
            destination = message.destination
            receiveMessage(clientMessage)
        }
    }
    //endregion

    private fun getResult(request: ClientRequest): String? {
        if (request is GetRequest) {
            return if (request.key in state)
                "VALUE ${request.key} ${state[request.key]}" else
                "NOT_FOUND"
        } else if (request is SetRequest) {
            state[request.key] = request.value
            return "STORED"
        } else if (request is DeleteRequest) {
            val found = (state.remove(request.key)) != null
            return if (found) "DELETED" else "NOT_FOUND"
        }
        return null
    }

    //region leaderToClient
    private fun handleClientRequest(message: ClientRequest) {
        messagesWithoutAnswer.add(message)
        if (status == Status.RECOVERING || status == Status.VIEWCHANGE) {
            return
        }
        NodeLogger.logProtocol("ClientRequest", isLeader())
        if (isLeader()) {
            NodeLogger.logMsgIn(message, message.clientId, isLeader(), status)
            if (message is GetRequest) {
                NodeLogger.logMsgIn(message, message.clientId, isLeader(), status)
                val result = if (message.key in state)
                    "VALUE ${message.key} ${state[message.key]}" else
                    "NOT_FOUND"
                if (true/*!doNotRequireAnswer.contains(message.requestNumber)*/) {
                    NodeLogger.logProtocol("Sent $result from $id to client${message.clientId}", isLeader())
                    if (destination == id) {
                        sendToClientAndDropFromHaveNotAnswer(message, message.clientId, result)
                    } else {
                        send(destination, SendResultToClientMessage(viewNumber, message.clientId, result, message))
                    }
                }
                destination = id
                NodeLogger.logProtocol(result, isLeader())
                return
            }
            opNumber++
            log.add(LogEntry(opNumber, message.requestNumber, message.clientId, message))
            val requestInfo = RequestInfo(message.requestNumber, message, null)
            lastRequestByClientId.put(message.clientId, requestInfo)
            clientIdByOpNumber.put(opNumber, message.clientId)
            allIds.forEachIndexed { i, it ->
                if (it != id) {
                    val msg = PrepareMessage(viewNumber, message.clientId, message.requestNumber,
                            message, opNumber, commitNumber)
                    send(it, msg)
                    NodeLogger.logMsgOut(msg, i, isLeader(), status)
                }
            }
        } else {
            NodeLogger.logMsgIn(message, message.clientId, isLeader(), status)
            val transferMsg = TransferClientMessage(id, message.clientId, message.requestNumber,
                    message.toString(), viewNumber)
            send(localLeaderId, transferMsg)
            NodeLogger.logMsgOut(message, localLeaderId, isLeader(), status)
        }
    }

    //endregion

    val doNotRequireAnswer = HashSet<Int>()

    //region viewChange
    private fun handleDoViewChangeMessage(message: DoViewChangeMessage) {
        NodeLogger.logProtocol("DoViewChangeMessage", isLeader())
        NodeLogger.logMsgIn(message, message.senderId, isLeader(), status)
        if (message.senderViewNumber > viewNumber) {
            println("here")
            startViewChange(message.senderViewNumber, id)
            doViewChangeNumberSet = HashSet<Int>()
        }
        if (doViewChangeNumberSet.size == 0) {
            println("there")
            doViewChangeInfo = DoViewChangeInfo(latestViewNumberWithNormalStatus, log, opNumber, commitNumber)
        }
        doViewChangeNumberSet.add(message.senderId)
        println(doViewChangeNumberSet.size)
        doViewChangeInfo.largestNormalStateViewNumber = Math.max(message.senderViewNumber,
                doViewChangeInfo.largestNormalStateViewNumber)
        doViewChangeInfo.largestOpNumber = Math.max(message.senderOpNumber, doViewChangeInfo.largestOpNumber)
        if (doViewChangeInfo.bestLog.size < message.log.size) {
            doViewChangeInfo.bestLog = message.log as ArrayList<LogEntry>
        }
        if (message.commitNumber > doViewChangeInfo.largestCommitNumber) {
            doViewChangeInfo.largestCommitNumber = message.commitNumber
        }
        if (doViewChangeNumberSet.size == GLOBAL_CONFIG.consensusSize) {
            doViewChangeNumberSet.clear()
            println("right there")
            viewNumber = message.senderViewNumber
            status = Status.NORMAL
            startViewChangeSet = HashSet()
            latestViewNumberWithNormalStatus = viewNumber
            allIds.forEach {
                if (it != id) send(it, StartViewMessage(viewNumber, doViewChangeInfo.bestLog
                        , opNumber, commitNumber, id))
            }
            localLeaderId = id
            if (commitNumber < doViewChangeInfo.bestLog.size) {
                val tempLog = doViewChangeInfo.bestLog.subList(commitNumber, doViewChangeInfo.bestLog.size)
                for (logEntry in tempLog) {
                    val clientMessage = logEntry.clientMessage
                    doNotRequireAnswer.add(clientMessage.requestNumber)
                    val requestInfo = RequestInfo(logEntry.requestNumber, clientMessage,
                            getResult(clientMessage))
                    lastRequestByClientId.put(logEntry.clientId, requestInfo)
                    clientIdByOpNumber.put(logEntry.opNumber, logEntry.clientId)
                    performClientRequest(requestInfo)
                }
            }
            commitNumber = doViewChangeInfo.largestCommitNumber
            /*  for (msg in messagesWithoutAnswer.toSet()) {
                  receiveMessage(msg)
              } */
            return
        }
    }

    private fun handleStartViewMessage(message: StartViewMessage) {
        NodeLogger.logProtocol("StartViewMessage", isLeader())
        NodeLogger.logMsgIn(message, message.leaderId, isLeader(), status)
        localLeaderId = message.leaderId
        viewNumber = message.viewNumber
        startViewChangeSet = HashSet()
        if (commitNumber < message.log.size) {
            val tempLog = message.log.subList(commitNumber, message.log.size)
            for (logMessage in tempLog) {
                doNotRequireAnswer.add(logMessage.requestNumber)
                val requestInfo = RequestInfo(logMessage.requestNumber, logMessage.clientMessage,
                        getResult(logMessage.clientMessage))
                lastRequestByClientId.put(logMessage.clientId, requestInfo)
                performClientRequest(requestInfo)
            }
        }
        /*  for (msg in messagesWithoutAnswer.toSet()) {
              receiveMessage(msg)
          } */
        status = Status.NORMAL
    }

    private fun startViewChange(newViewNumber: Int, newLeader: Int): Unit {
        status = Status.VIEWCHANGE
        viewNumber = newViewNumber
        allIds.forEach { it ->
            if (it != id) {
                val startViewChangeMessage = StartViewChangeMessage(viewNumber, newLeader, id)
                send(it, startViewChangeMessage)
                NodeLogger.logMsgOut(startViewChangeMessage, it, isLeader(), status)
            }
        }
    }

    private fun handleStartViewChangeMessage(message: StartViewChangeMessage) {
        NodeLogger.logProtocol("StartViewChangeMessage", isLeader())
        NodeLogger.logMsgIn(message, message.senderId, isLeader(), status)
        if (viewNumber < message.newViewNumber) {
            startViewChange(message.newViewNumber, message.newLeaderId)
            startViewChangeSet = HashSet<Int>()
            startViewChangeSet.add(message.senderId)
            return
        } else if (viewNumber == message.newViewNumber) {
            startViewChangeSet.add(message.senderId)
            if (startViewChangeSet.size == GLOBAL_CONFIG.consensusSize - 1) {
                val doViewChangeMessage = DoViewChangeMessage(viewNumber, log,
                        latestViewNumberWithNormalStatus,
                        opNumber, commitNumber, id)
                NodeLogger.logMsgOut(doViewChangeMessage, message.newLeaderId, isLeader(), status)
                send(message.newLeaderId, doViewChangeMessage)
            }
        }
    }

    //endregion

    var nonce: Long = 0
    //region recovery
    private fun handleRecoveryResponse(leaderId: Int, message: RecoveryResponse) {
        if (status == Status.RECOVERING && message.nonce == nonce) {
            NodeLogger.logProtocol("RecoveryResponse", isLeader())
            recoveryResponses++
            if (message.log != null && message.viewNumber >= viewNumber) {
                localLeaderId = leaderId
                viewNumber = message.viewNumber
                doneRecovering = true
                if (commitNumber < message.log.size) {
                    val tempLog = message.log.subList(commitNumber, message.log.size)
                    for (logEntry in tempLog) {
                        val requestInfo = RequestInfo(logEntry.requestNumber, logEntry.clientMessage, null)
                        lastRequestByClientId.put(logEntry.clientId,
                                requestInfo)
                        performClientRequest(requestInfo)
                        val msgToLeader = PrepareOKMessage(viewNumber, opNumber, id)
                        clientIdByOpNumber.put(logEntry.opNumber, logEntry.clientId)
                        send(localLeaderId, msgToLeader)
                        NodeLogger.logMsgOut(msgToLeader, localLeaderId, isLeader(), status)
                    }
                }
                opNumber = message.opNumber!!
                commitNumber = message.commitNumber!!
            }
            NodeLogger.logProtocol("$recoveryResponses = recoveryResponses, ${log.size}," +
                    "$oldCommitNumber ", isLeader())
            if (recoveryResponses >= GLOBAL_CONFIG.consensusSize && doneRecovering) {
                doneRecovering = false
                /*  for (msg in messagesWithoutAnswer.toSet()) {
                      receiveMessage(msg)
                  }*/
                status = Status.NORMAL
            }
        }
    }

    private fun handleRecoveryMessage(message: RecoveryMessage) {
        if (status == Status.NORMAL) {
            NodeLogger.logProtocol("RecoveryMessage", isLeader())
            if (message.id != id && status == Status.NORMAL) {
                if (isLeader()) {
                    send(message.id, RecoveryResponse(viewNumber, message.nonce, log, opNumber, commitNumber,
                            localLeaderId))
                } else {
                    send(message.id, RecoveryResponse(viewNumber, message.nonce, null, null, null, localLeaderId))
                }
            }
        }
    }

    //endregion
}